from django.db import models

# Create your models here.
class React(models.Model):
    name = models.CharField(max_length = 30)
    price = models.CharField(max_length= 30)
    description = models.TextField()
    category = models.CharField(max_length=30)
    display = models.ImageField(upload_to='images')


class User(models.Model):
    username = models.CharField(max_length=40)
    password = models.CharField(max_length=30)
    email = models.CharField(max_length = 100)

    