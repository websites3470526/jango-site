from rest_framework import serializers
from .models import *

class ReactSerializer(serializers.ModelSerializer):
    photo_url = serializers.SerializerMethodField()

    class Meta:
        model = React
        fields = ['name', 'price', 'description', 'category', 'photo_url']


    def get_photo_url(self, react):
        request = self.context.get('request')
        photo_url = react.display.url
        return request.build_absolute_uri(photo_url)
    
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'password', 'email']