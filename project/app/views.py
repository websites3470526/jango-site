from django.shortcuts import render
from rest_framework.views import APIView
from . models import *
from . serializer import *
from rest_framework.response import Response
from django.http import HttpResponse
from rest_framework import status
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
class ReactView(APIView):

    serializer_class = ReactSerializer

    Headers = {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "POST, GET, PUT",
        "Access-Control-Allow-Headers": "Content-Type",
    }

    def get(self, request):
        qs = React.objects.all()
        return Response(ReactSerializer(qs, many=True, context={'request': request}).data)
        
    
    def post(self, request):
        serializer = ReactSerializer(data = request.data)
        if serializer.is_valid(raise_exception = True):
            serializer.save()
            return Response(serializer.data)

    def delete(self, request):
        for output in React.objects.all():
            output.delete()
        return Response()
    

class SignupView(APIView):
    serializer_class = UserSerializer
    
    def get(self, request):
        qs = User.objects.all()
        return Response(UserSerializer(qs, many=True, context={'request': request}).data)
    #@csrf_exempt
    def post(self, request):
        serializer = UserSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            user = User.objects.create(username = request.data['username'])
            user.set_password(request.data['password'])
            user.save()
            token = Token.objects.create(user = user)
            return Response({"token": token.key, "user": serializer.data})
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

    
    
class LoginView(APIView):
    def test_token(self, request):
        return Response({})
    
    def login(self, requst):
        return Response({})